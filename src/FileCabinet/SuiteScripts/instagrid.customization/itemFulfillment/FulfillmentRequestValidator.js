/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        '../common/constants',
        '../dao/transaction',
    ],
    (
        record,
        constants,
        transactionDAO
    ) => {

        function FulfillmentRequestValidator (requestBody) {
            this.request = requestBody;
            this.itemFulfillment = null;
            this.salesOrderId = null;
            this.fulfillmentRequestId = null;
            this.errors = [];
        }

        FulfillmentRequestValidator.prototype.validateRequest = function () {

            this._validateTransactionLinks();
            this._validateItems();

            if (this.errors.length > 0) {
                return this;
            }

            this._transformOrderForValidation();
            this._validateLineNumReferences();

            if (this.errors.length > 0) {
                return this;
            }

            this._validateInventoryDetailsData();

            return this;
        }

        /**
         * The function transforms Sales Order into Item Fulfillment with pre-defaulted Fulfillment Request reference
         * (in case it exists) for record validation.
         * @private
         */
        FulfillmentRequestValidator.prototype._transformOrderForValidation = function () {
            this.itemFulfillment = record.transform({
                fromId: this.salesOrderId,
                fromType: record.Type.SALES_ORDER,
                toType: record.Type.ITEM_FULFILLMENT,
                isDynamic: true,
                defaultValues: (this.fulfillmentRequestId) ? { "fftreqid" :  this.fulfillmentRequestId } : null,
            });
        }

        /**
         * The function validates that request contains proper items data including references to Item Fulfillment lines.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateItems = function () {
            if (!this.request[constants.REQUEST_BODY_PARAMS.ITEM] ||
                !this.request[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS] ||
                this.request[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS].length < 1) {
                this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.MISSING_ITEM_DATA);
                return;
            }

            this._validateItemLinks();
        }

        /**
         * The function iterates over all the items data in the request and validates that all of them contains proper
         * reference to an Item Fulfillment line - through line number, item name or item ID.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateItemLinks = function () {
            let items = this.request[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS];

            for (let i = 0; i < items.length; i++) {
                if (items[i][constants.REQUEST_BODY_PARAMS.LINE] === undefined &&
                    (!items[i][constants.REQUEST_BODY_PARAMS.ITEM] ||
                        !items[i][constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.INTERNAL_ID]) &&
                    !items[i][constants.REQUEST_BODY_PARAMS.ITEM_NAME]) {
                    this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.ITEM_REFERENCE_MISSING + i);
                }
            }
        }

        /**
         * The function validates references in the request for Sales Order and Fulfillment Request records and
         * initializes their Internal IDs.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateTransactionLinks = function () {

            this._validateAndInitializeSalesOrderRef();

            if (this.errors.length) {
                return;
            }

            this._validateAndInitializeFulfilRequestRef();
        }

        /**
         * The function validates that the request contains proper reference to Sales Order through Internal/External ID
         * and pulls the Internal ID in case External ID was provided.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateAndInitializeSalesOrderRef = function () {
            if (!this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_ID] &&
                !this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_EXT_ID]) {
                this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.SALES_ORDER_REF_MISSING);
            }
            else if (!this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_ID] &&
                this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_EXT_ID]) {
                this.salesOrderId = transactionDAO.getTransactionIdFromExternalId(
                    this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_EXT_ID]);
                if (!this.salesOrderId) {
                    this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.SALES_ORDER_EXT_ID_NOT_FOUND);
                }
            }
            else {
                this.salesOrderId = this.request[constants.REQUEST_BODY_PARAMS.SALES_ORDER_ID];
            }
        }

        /**
         * The function checks if Fulfillment Request (FR) exist for current Sales Order and validates that the request
         * contains proper reference to FR. It will also pull FR's Internal ID in case External ID was provided.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateAndInitializeFulfilRequestRef = function () {
            if (!this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_ID] &&
                !this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_EXT_ID] &&
                transactionDAO.getFulfillmentRequestForSalesOrder(this.salesOrderId)) {
                this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.FUL_REQ_EXISTS_NOT_DEFINED);
            }
            else if (!this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_ID] &&
                this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_EXT_ID]) {
                this.fulfillmentRequestId = transactionDAO.getTransactionIdFromExternalId(
                    this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_EXT_ID]);
                if (!this.fulfillmentRequestId) {
                    this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.FULFILL_REQ_EXT_ID_NOT_FOUND);
                }
            }
            else {
                this.fulfillmentRequestId = this.request[constants.REQUEST_BODY_PARAMS.FULFILLMENT_REQUEST_ID];
            }
        }

        /**
         * The function validates that highest referenced line number in the request does not exceed the total amount
         * of lines on the Item Fulfillment (i.e. it does not reference non-existing line).
         * @private
         */
        FulfillmentRequestValidator.prototype._validateLineNumReferences = function () {
            let itemFulfillmentLineCount = this.itemFulfillment.getLineCount({
                sublistId: 'item'
            });

            let highestReferencedLineInRequest = this._getHighestReferencedLineInRequest();

            if (highestReferencedLineInRequest > itemFulfillmentLineCount) {
                this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.LINE_DOES_NOT_EXIST);
            }
        }

        /**
         * The function searches for the highest referenced Item Fulfillment line in the request and returns it.
         * @returns {number} highestReferencedLine
         * @private
         */
        FulfillmentRequestValidator.prototype._getHighestReferencedLineInRequest = function () {
            let highestReferencedLine = 0;
            let items = this.request[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS];

            for (let i = 0; i < items.length; i++) {
                highestReferencedLine = (items[i][constants.REQUEST_BODY_PARAMS.LINE] > highestReferencedLine) ?
                    items[i][constants.REQUEST_BODY_PARAMS.LINE] :
                    highestReferencedLine;
            }

            return (highestReferencedLine + 1);
        }

        /**
         * The function iterates over Item Fulfillment lines and validates that all items data in the request contains
         * Inventory Detail data for the lines that require it. It also validates that Lot Number quantity in the request
         * is populated for Lot Numbered items.
         * @private
         */
        FulfillmentRequestValidator.prototype._validateInventoryDetailsData = function () {
            let items = this.request[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS];

            for (let i = 0; i < this.itemFulfillment.getLineCount('item'); i++) {
                this.itemFulfillment.selectLine({
                    sublistId: 'item',
                    line: i,
                });

                let itemData = this._findItemInRequest(items, i);

                if (itemData) {
                    if (this._isInventoryDetailRequiredButMissing(itemData)) {
                        this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.INVENTORY_DETAIL_REQ + i);
                    }
                    else if (this._isLotNumberedItemButQtyMissing(itemData)) {
                        this.errors.push(constants.ERROR_MSG.REQUEST_VALIDATOR.LOT_NUM_QTY_NOT_DEFINED + i);
                    }
                }
            }
        }

        /**
         * The function takes information from currently selected line, searches for the same item in the request and
         * returns it.
         * @param items - request items data
         * @returns {object} Item
         * @private
         */
        FulfillmentRequestValidator.prototype._findItemInRequest = function (items, lineNum) {
            let itemId = parseInt(this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item'
            }));
            let itemName = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'itemname'
            });

            for (let i = 0; i < items.length;i++) {
                if (lineNum === items[i][constants.REQUEST_BODY_PARAMS.LINE] ||
                    (items[i][constants.REQUEST_BODY_PARAMS.ITEM] &&
                    itemId === items[i][constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.INTERNAL_ID]) ||
                    itemName === items[i][constants.REQUEST_BODY_PARAMS.ITEM_NAME]) {
                    return items[i];
                }
            }

            return;
        }

        /**
         * The function checks if the inventory detail is required on the Item Fulfillment line and if the inventory
         * detail data are missing in the request - returns true if both conditions are met.
         * @param lineData - line item data from the request
         * @returns {boolean}
         * @private
         */
        FulfillmentRequestValidator.prototype._isInventoryDetailRequiredButMissing = function (lineData) {
            let inventoryDetailRequired = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'inventorydetailreq'
            });

            let isLotItem = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'islotitem'
            });

            let isSerialItem = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isserialitem'
            });

            return (inventoryDetailRequired === 'T' || isLotItem || isSerialItem) &&
                (!lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL] ||
                 !lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL][constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT] ||
                 !lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL][constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT]
                     [constants.REQUEST_BODY_PARAMS.ITEMS] ||
                 lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL][constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT]
                     [constants.REQUEST_BODY_PARAMS.ITEMS].length === 0);
        }

        /**
         * The function checks if the current line item is lot numbered and if the lot number quantity in the request
         * is missing - returns true if both conditions are met.
         * @param lineData - line item data from the request
         * @returns {boolean}
         * @private
         */
        FulfillmentRequestValidator.prototype._isLotNumberedItemButQtyMissing = function (lineData) {
            let isNumbered = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isnumbered'
            });
            if (!isNumbered) {
                isNumbered = this.itemFulfillment.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'islotitem'
                });
            }

            let isSerial = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isserial'
            });
            if (!isSerial) {
                isSerial = this.itemFulfillment.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'isserialitem'
                });
            }

            if ((isNumbered === 'T' && isSerial === 'F') ||
                (isNumbered && !isSerial)){
                let lotNumbers = lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL][constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT]
                    [constants.REQUEST_BODY_PARAMS.ITEMS];
                for (let i = 0; i < lotNumbers.length; i++) {
                    if (!lotNumbers[i][constants.REQUEST_BODY_PARAMS.QUANTITY]) {
                        return true;
                    }
                }
            }

            return false;
        }

        return {Instance: FulfillmentRequestValidator};
    });
