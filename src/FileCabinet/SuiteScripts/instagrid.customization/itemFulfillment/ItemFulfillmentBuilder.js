/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        '../common/constants',
        '../common/application',
    ],
    (
        record,
        constants,
        application
    ) => {

        function ItemFulfillmentBuilder (fulfillmentData) {
            this.fulfillmentData = fulfillmentData;
            this.itemFulfillment = null;
            this.itemFulfillmentId = null;
        }

        ItemFulfillmentBuilder.prototype.initializeValidatedRecord = function (validatedRecord) {
            this.itemFulfillment = validatedRecord;
            return this;
        }

        ItemFulfillmentBuilder.prototype.createItemFulfillment = function () {

            this._setHeaderFields();
            this._configureLines();
            this._configurePackages();
            this._saveItemFulfillment();

            return this;
        }

        ItemFulfillmentBuilder.prototype._setHeaderFields = function () {
            application.setValueIfDefined(this.itemFulfillment, 'customform', this.fulfillmentData[constants.REQUEST_BODY_PARAMS.CUSTOM_FORM]);
            application.setValueIfDefined(this.itemFulfillment, 'trandate',
                application.getFormattedDate(this.fulfillmentData[constants.REQUEST_BODY_PARAMS.TRANSACTION_DATE]));

            if (this.fulfillmentData[constants.REQUEST_BODY_PARAMS.SHIP_STATUS]) {
                this.itemFulfillment.setValue({
                    fieldId: 'shipstatus',
                    value: this.fulfillmentData[constants.REQUEST_BODY_PARAMS.SHIP_STATUS][constants.REQUEST_BODY_PARAMS.ID]
                });
            }
        }

        ItemFulfillmentBuilder.prototype._configureLines = function () {
            let items = this.fulfillmentData[constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.ITEMS];

            for (let i = 0; i < this.itemFulfillment.getLineCount('item'); i++) {
                this.itemFulfillment.selectLine({
                    sublistId: 'item',
                    line: i,
                });

                let itemData = this._findItemInRequest(items, i);

                if (itemData) {
                    this._setLineItemFields(itemData);
                }
                else {
                    this.itemFulfillment.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: false,
                    });
                }

                this.itemFulfillment.commitLine({
                    sublistId: 'item'
                });
            }
        }

        ItemFulfillmentBuilder.prototype._findItemInRequest = function (items, lineNum) {
            let itemId = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item'
            });
            let itemName = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'itemname'
            });

            for (let i = 0; i < items.length;i++) {
                if (lineNum === items[i][constants.REQUEST_BODY_PARAMS.LINE] ||
                    (items[i][constants.REQUEST_BODY_PARAMS.ITEM] &&
                        parseInt(itemId) === items[i][constants.REQUEST_BODY_PARAMS.ITEM][constants.REQUEST_BODY_PARAMS.INTERNAL_ID]) ||
                    itemName === items[i][constants.REQUEST_BODY_PARAMS.ITEM_NAME]) {
                    return items[i];
                }
            }

            return;
        }

        ItemFulfillmentBuilder.prototype._setLineItemFields = function (lineData) {
            application.setCurrentSublistValueIfDefined(this.itemFulfillment, 'item', 'itemreceive', true);
            application.setCurrentSublistValueIfDefined(this.itemFulfillment, 'item', 'quantity', lineData[constants.REQUEST_BODY_PARAMS.QUANTITY]);

            if (this._isInventoryDetailProvided(lineData)) {
                this._configureInventoryDetail(lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL]);
            }
        }

        /**
         * Loads and configures Inventory Detail subrecord. If subrecord is not available, serial/lot numbers are entered
         * into multiselect field (which replaces Inventory Detail subrecord).
         * @param inventoryDetail
         * @private
         */
        ItemFulfillmentBuilder.prototype._configureInventoryDetail = function (inventoryDetail) {
            let inventoryDetailSubrec;
            try {
                inventoryDetailSubrec = this.itemFulfillment.getCurrentSublistSubrecord({
                    sublistId: 'item',
                    fieldId: 'inventorydetail',
                });
            }
            catch (e) {
                this._configureMultiselectInvDetail(inventoryDetail);
                return;
            }

            let serialLotNumbers = inventoryDetail[constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT]
                [constants.REQUEST_BODY_PARAMS.ITEMS];

            if (inventoryDetailSubrec) {
                for (let i = 0; i < serialLotNumbers.length; i++) {
                    inventoryDetailSubrec.selectNewLine({
                        sublistId: 'inventoryassignment',
                    });


                    if (serialLotNumbers[i][constants.REQUEST_BODY_PARAMS.ISSUE_INVENTORY_NUMBER]
                        [constants.REQUEST_BODY_PARAMS.ID]) {
                        inventoryDetailSubrec.setCurrentSublistValue({
                            sublistId: 'inventoryassignment',
                            fieldId: 'issueinventorynumber',
                            value: serialLotNumbers[i][constants.REQUEST_BODY_PARAMS.ISSUE_INVENTORY_NUMBER]
                                [constants.REQUEST_BODY_PARAMS.ID],
                        });
                    }
                    else {
                        inventoryDetailSubrec.setCurrentSublistText({
                            sublistId: 'inventoryassignment',
                            fieldId: 'issueinventorynumber',
                            text: serialLotNumbers[i][constants.REQUEST_BODY_PARAMS.ISSUE_INVENTORY_NUMBER]
                                [constants.REQUEST_BODY_PARAMS.INVENTORY_NUMBER],
                        });
                    }

                    application.setCurrentSublistValueIfDefined(inventoryDetailSubrec, 'inventoryassignment',
                        'quantity', serialLotNumbers[i][constants.REQUEST_BODY_PARAMS.QUANTITY]);

                    inventoryDetailSubrec.commitLine({sublistId: 'inventoryassignment'});
                }
            }
        }

        /**
         * Configures Inventory Detail through multiselect field on transaction line instead of subrecord.
         * @param inventoryDetail
         * @private
         */
        ItemFulfillmentBuilder.prototype._configureMultiselectInvDetail = function (inventoryDetail) {
            let serialLotNumbersData = inventoryDetail[constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT]
                [constants.REQUEST_BODY_PARAMS.ITEMS];

            let isLotItem = this.itemFulfillment.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'islotitem'
            });

            let serialLotNumbers = [];

            for (let i = 0; i < serialLotNumbersData.length; i++) {
                let serialLotNumber = serialLotNumbersData[i][constants.REQUEST_BODY_PARAMS.ISSUE_INVENTORY_NUMBER]
                    [constants.REQUEST_BODY_PARAMS.INVENTORY_NUMBER];
                if (isLotItem) {
                    serialLotNumber += '(' + serialLotNumbersData[i][constants.REQUEST_BODY_PARAMS.QUANTITY] + ')';
                }
                serialLotNumbers.push(serialLotNumber);
            }

            this.itemFulfillment.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'serialnumbers',
                value: serialLotNumbers,
            });
        }

        ItemFulfillmentBuilder.prototype._configurePackages = function () {
            if (this.fulfillmentData[constants.REQUEST_BODY_PARAMS.PACKAGE] &&
                this.fulfillmentData[constants.REQUEST_BODY_PARAMS.PACKAGE][constants.REQUEST_BODY_PARAMS.ITEMS]) {

                let packages = this.fulfillmentData[constants.REQUEST_BODY_PARAMS.PACKAGE][constants.REQUEST_BODY_PARAMS.ITEMS];
                let currentPackageLineCount = parseInt(this.itemFulfillment.getLineCount('package'));

                if (!currentPackageLineCount) {
                    currentPackageLineCount = 0;
                }

                for (let i = 0; i < packages.length; i++) {

                    if (i < currentPackageLineCount) {
                        this.itemFulfillment.selectLine({
                            sublistId: 'package',
                            line: i,
                        });
                    }
                    else {
                        this.itemFulfillment.selectNewLine({
                            sublistId: 'package'
                        });
                    }

                    application.setCurrentSublistValueIfDefined(this.itemFulfillment, 'package',
                        'packageweight', packages[i][constants.REQUEST_BODY_PARAMS.PACKAGE_WEIGHT]);

                    application.setCurrentSublistValueIfDefined(this.itemFulfillment, 'package',
                        'packagetrackingnumber', packages[i][constants.REQUEST_BODY_PARAMS.PACKAGE_TRACK_NUM]);

                    application.setCurrentSublistValueIfDefined(this.itemFulfillment, 'package',
                        'packagedescr', packages[i][constants.REQUEST_BODY_PARAMS.PACKAGE_DESC]);

                    this.itemFulfillment.commitLine({
                        sublistId: 'package'
                    });
                }
            }
        }

        ItemFulfillmentBuilder.prototype._saveItemFulfillment = function () {
            this.itemFulfillmentId = this.itemFulfillment.save();
        }

        ItemFulfillmentBuilder.prototype._isInventoryDetailProvided = function (lineData) {
            return lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL] &&
                lineData[constants.REQUEST_BODY_PARAMS.INVENTORY_DETAIL][constants.REQUEST_BODY_PARAMS.INVENTORY_ASSIGNMENT];
        }

        return {Instance: ItemFulfillmentBuilder};
    });
