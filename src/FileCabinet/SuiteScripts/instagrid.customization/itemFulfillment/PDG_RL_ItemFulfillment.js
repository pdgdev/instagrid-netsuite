/**
 * @NApiVersion 2.1
 * @NScriptType Restlet
 */
define(['N/record', '../common/constants', './FulfillmentRequestValidator', './ItemFulfillmentBuilder'],
    /**
 * @param{record} record
 */
    (record,
     constants,
     fulfillReqValidator,
     itemFulfillmentBuilder
    ) => {

        /**
         * Defines the function that is executed when a POST request is sent to a RESTlet.
         * @param {string | Object} requestBody - The HTTP request body; request body is passed as a string when request
         *     Content-Type is 'text/plain' or parsed into an Object when request Content-Type is 'application/json' (in which case
         *     the body must be a valid JSON)
         * @returns {string | Object} HTTP response body; returns a string when request Content-Type is 'text/plain'; returns an
         *     Object when request Content-Type is 'application/json' or 'application/xml'
         * @since 2015.2
         */
        const post = (requestBody) => {

            let LOGGING_TITLE = '<< post >>';

            try {
                let fulfillmentValidator = new fulfillReqValidator.Instance(requestBody)
                    .validateRequest();

                let validationErrors = fulfillmentValidator.errors;

                if (validationErrors.length > 0) {
                    return { validationErrors };
                }

                let fulfillmentBuilder = new itemFulfillmentBuilder.Instance(requestBody)
                    .initializeValidatedRecord(fulfillmentValidator.itemFulfillment)
                    .createItemFulfillment();

                let itemFulfillmentId = fulfillmentBuilder.itemFulfillmentId;

                return {itemFulfillmentId};
            }
            catch (error) {
                log.error('<< post >>', error);
                let errorMsg = error.message;
                return {errorMsg};
            }
        }

        return {post}

    });
