define(['N/format'], function (format) {
    const setValueIfDefined = (record, fieldId, value) => {
        if (value) {
            record.setValue({
                fieldId: fieldId,
                value: value
            });
        }
    };

    const setCurrentSublistValueIfDefined = (record, sublistId, fieldId, value) => {
        if (value) {
            record.setCurrentSublistValue({
                sublistId: sublistId,
                fieldId: fieldId,
                value: value
            });
        }
    }

    const getFormattedDate = function (rawDate) {
        if (rawDate) {
            let formattedDate = new Date(rawDate);
            if (formattedDate) {
                let timezoneOffset = formattedDate.getTimezoneOffset() * 60000;
                let correctedDate = new Date(formattedDate.getTime() + timezoneOffset);
                return correctedDate;
            }
        }

        return;
    }

    return {
        setValueIfDefined,
        setCurrentSublistValueIfDefined,
        getFormattedDate,
    };
});
