define(
    ['N/search', '../common/constants', '../common/application'],
    function (search, constants, application) {

        function getFulfillmentRequestForSalesOrder (salesOrderId) {
            let fulfillmentRequestSearch = search.create({
                type: search.Type.FULFILLMENT_REQUEST,
                filters: [
                    ['createdfrom', 'anyof', salesOrderId]
                ],
                columns: [
                    'internalid'
                ]
            });

            let fulfillmentRequestId;

            fulfillmentRequestSearch.run().each(function(result){
                fulfillmentRequestId = result.getValue('internalid');
            });

            return fulfillmentRequestId;
        }

        function getTransactionIdFromExternalId(externalId) {
            let transactionSearch = search.create({
                type: search.Type.TRANSACTION,
                filters: [
                    ['externalid', 'is', externalId]
                ],
                columns: [
                    'internalid'
                ]
            });

            let transactionId;

            transactionSearch.run().each(function(result){
                transactionId = result.getValue('internalid');
            });

            return transactionId;
        }

        return {
            getFulfillmentRequestForSalesOrder,
            getTransactionIdFromExternalId,
        };
    }
);